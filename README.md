# Etorch

#### 介绍
易语言移植pytorch项目，名称为Etorch

#### 软件架构
参考pytorch设计，自动求导。易于扩展。

#### 安装教程

1. 安装易语言
2. 下载源码etorch.e
3. 打开源码
4. gpu dll或openblas dll依赖请进QQ群69947835获取。

#### 参与贡献

感谢噫吁嚱QQ2597922741 提供帮助。

### demo1:  
![image](demo1.png)

### demo2:  
![image](demo2.png)